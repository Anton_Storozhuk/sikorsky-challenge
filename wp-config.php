<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'database' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'y`ggeSBI%a(FAJI=JCk+SIEk{*o4yRl?6ijO*<?d4$*RYU*ePbt}]lQWASO?,uhz' );
define( 'SECURE_AUTH_KEY',  'Q9G4Zl4pCF]$mQYFcJDFzk.KsI[8n$M-<G3sQ;tam}LR{>-xHUkT7ZRmKI9o5I?{' );
define( 'LOGGED_IN_KEY',    '<=PR1<J#N1KEOz(^tFkuysxIj4E !Y^ +HRAI=5U1Qa]Ku0d4({^oFPt-$2jb-0}' );
define( 'NONCE_KEY',        '<_EG5Q9:y!B_DgEHKl/q2B6bkqKiQdXOIZ[.n,}.z9i/7E8Q{XaFv Z]FF+~GW,)' );
define( 'AUTH_SALT',        'W=>(ua6t?A+9GF}8.-JvIE^Zt8U To[/;|{@82 D|QhW(atv8U)ThuqB7jO#>[^p' );
define( 'SECURE_AUTH_SALT', 'H(ZW.VA>11>ONc|ixZR0rPr) cA0}hK9K_fesN3-Tres,{_r60)QuLF(M{` @~go' );
define( 'LOGGED_IN_SALT',   '63fi@14*gfDzEn7A*#NFboa*|5ZiH[D_U{3?yrY%-vz.a]./sv;^dOthXEua]1bm' );
define( 'NONCE_SALT',       'I%Y,,H>b,~{/^i*Xq~^ag1#N81nZ)b#iI(bLIZ1/7gNtsT_:JmT.1;on*^?BX@GH' );
define( 'WPCF7_AUTOP',      false ); 

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */
define( 'HEADLESS_FRONTEND_URL', 'http://localhost:3000/' );
define( 'PREVIEW_SECRET_TOKEN', 'ANY_RANDOM_STRING');
define( 'GRAPHQL_JWT_AUTH_SECRET_KEY', 'ANY_RANDOM_STRING' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
