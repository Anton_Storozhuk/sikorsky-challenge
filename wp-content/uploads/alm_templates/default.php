<article class="article">
	<a 
       class="
			<?= in_category( 'projects' ) ? 
				'article__image article__image--project' : 
				'article__image' 
			?>"
       href="<?= the_permalink() ?>">
		<?= the_post_thumbnail() ?>
	</a>
	<div class="article__text-container">
		<div class="article__categories">
			<?= the_category(' | ') ?>
		</div>
		<h2 class="article__title">
			<a href="<?= the_permalink() ?>">
				<?= get_the_title() ?>
			</a>
		</h2>
		<p class="article__text">
			<?= get_the_excerpt() ?>
		</p>
		<div class="article__footer">
			<p class="article__date">
				<?= the_time(get_option("date_format")) ?>
			</p>
			<a class="article__button" href="<?= the_permalink() ?>">
				<?= get_field('read_more_button', 'theme-general-settings') ?>
				<i>
					<svg width="8" height="10" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M-5.01952e-07 1.51667L4.97297 6.5L-6.62956e-08 11.4833L1.51351 13L8 6.5L1.51351 -6.61578e-08L-5.01952e-07 1.51667Z" fill="#e90007"/>
					</svg>
				</i>
			</a>
		</div>
	</div>
</article>