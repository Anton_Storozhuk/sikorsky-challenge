<?php
    /*
        Template Name: Home Page Template
    */

    get_header();

    $success_stories = get_field('success_stories', get_the_ID());
    $registration = get_field('registration', get_the_ID());
    $news = get_field('news', get_the_ID());
    $video = get_field('video', get_the_ID());
    $learning = get_field('learning', get_the_ID());
    $information = get_field('information', get_the_ID());
    $managment = get_field('managment', get_the_ID());
    $sponsors = get_field('sponsors', get_the_ID());
?>

<section class="banner">
    <div class="wrapper">
        <div class="swiper-container main-slider">
			<div class="swiper-wrapper">
				<?php 
					$counter = 0;
					foreach ($success_stories as $key => $item) :
					$counter++;
				?>
					<div class="swiper-slide main-slider__item">
                        <div class="main-slider__text-container">
							<div class="main-slider__text">
								<?php if ( $counter == 1 ) : ?>
									<h1 class="main-slider__title">
                                        <span>
                                            <?= $item['title'] ?>
                                        </span>
                                    </h1>
								<?php else : ?>
									<p class="h1-title main-slider__title">
                                        <span>
                                            <?= $item['title'] ?>
                                        </span>
                                    </p>
								<?php endif; ?>
								<p class="main-slider__text"><?= $item['text'] ?></p>
							</div>
							<a 
                                href="<?= $item['button']['url'] ?>" 
                                class="btn btn-second main-slider__button"
                            >
								<?= $item['button']['title'] ?>
                            </a>
						</div>
						<div class="main-slider__image">
							<img
								class="no-lazyload"
								src="<?= $item['image']['url'] ?>" 
								alt="<?= $item['image']['alt'] ?>"
							>
						</div>
					</div>
                <?php endforeach; ?>
			</div>
			<div class="swiper-pagination main-slider-pagination"></div>
		</div>
    </div>
</section>

<section class="registration">
    <div class="wrapper">
        <div class="text-container registration__text">
            <?= $registration['text'] ?>
        </div>
        <a class="btn btn-main registration__button" href="<?= $registration['button']['url'] ?>" target="_blank">
            <?= $registration['button']['title'] ?>
        </a>
    </div>
    <img class="registration__image" src="<?= $registration['image']['url'] ?>" alt="<?= $registration['image']['alt'] ?>">
</section>

<section>
    <div class="wrapper">
        <h2>
            <span>
                <?= $news['title'] ?>
            </span>
        </h2>
        <div class="news">
            <?php 
                $recent_news = new WP_Query(array(
                    'category_name'=>'news', 
                    'post_type' => 'post', 
                    'post_status' =>'publish', 
                    'posts_per_page' => 3, 
                    'order' => 'DESC',
                ));  
            ?>
            <?php if ($recent_news->have_posts()) : ?>
                <?php 
                    while ($recent_news->have_posts()) :
                    $recent_news->the_post();
                ?>
                    <article class="article">
                        <a class="article__image" href="<?= the_permalink() ?>">
                            <?= the_post_thumbnail() ?>
                        </a>
                        <div class="article__text-container">
                            <div class="article__categories">
                                <?= the_category(' | ') ?>
                            </div>
                            <h3 class="article__title">
                                <a href="<?= the_permalink() ?>">
                                    <?= get_the_title() ?>
                                </a>
                            </h3>
                            <p class="article__text"><?= get_the_excerpt() ?></p> 
                            <div class="article__footer">
                                <p class="article__date">
                                    <?= the_time(get_option("date_format")) ?>
                                </p>
                                <a class="article__button" href="<?= the_permalink() ?>">
                                    <?= get_field('read_more_button', 'theme-general-settings') ?>
                                    <i>
                                        <svg width="8" height="10" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M-5.01952e-07 1.51667L4.97297 6.5L-6.62956e-08 11.4833L1.51351 13L8 6.5L1.51351 -6.61578e-08L-5.01952e-07 1.51667Z" fill="#e90007"/>
                                        </svg>
                                    </i>
                                </a>
                            </div>
                        </div>
                    </article>
                <?php endwhile ?>
            <?php 
                wp_reset_postdata();
                endif; 
            ?>
        </div>
        <a class="btn btn-second" href=<?= $news['button']['url'] ?>>
            <?= $news['button']['title'] ?>
        </a>
    </div>
</section>

<section class="video">
    <div class="wrapper">
        <h2>
            <span>
                <?= $video['title'] ?>
            </span>
        </h2>
        <div 
            class="video-wrapper"
            data-aos="zoom-in"
        >
            <?= $video['video'] ?>
        </div>
    </div>
</section>

<section class="learning">
    <div class="wrapper">
        <h2>
            <span>
                <?= $learning['title'] ?>
            </span>
        </h2>
        <div class="learning__content">
            <div class="text-container learning__text-container">
                <?= $learning['text'] ?>
            </div>
            <img 
                class="learning__image" 
                src="<?php echo get_template_directory_uri(); ?>/assets/images/cubic-rubic.gif"
                alt="<?= $learning['image']['alt'] ?>"
                data-aos="fade-left"
            >
        </div>
    </div>
</section>

<section class="information">
    <div class="wrapper information__content">
        <img 
            class="information__photo" 
            src="<?= $information['photo']['url'] ?>" 
            alt="<?= $information['photo']['alt'] ?>"
            data-aos="fade-right"
        >
        <div class="text-container information__text-container">
            <?= $information['text'] ?>
        </div>
    </div>
</section>

<section>
    <div class="wrapper">
        <h2>
            <span>
                <?= $managment['title'] ?>
            </span>
        </h2>
        <div class="organizators">
            <?php foreach( $managment['organizators'] as $key => $person ) : ?>
                <div 
                    class="organizator"
                    data-aos="zoom-in"
                    data-aos-delay="<?= 300 + $key * 150 ?>"
                >
                    <img 
                        src="<?= $person['photo']['url'] ?>" 
                        alt="<?= $person['photo']['alt'] ?>"
                    >
                    <div class="organizator__text-container">
                        <p>
                            <strong>
                                <?= $person['name'] ?>
                            </strong>
                        </p>
                        <p><?= $person['position'] ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>   
    </div>
</section>

<section>
    <div class="wrapper">
        <h2>
            <span>
                <?= $sponsors['title'] ?>
            </span>
        </h2>
        <div class="sponsors">
            <?php foreach( $sponsors['list'] as $key => $sponsor ) : ?>
                <?php if($sponsor['link']) : ?>
                    <a 
                        class="sponsor"
                        data-aos="zoom-in"
                        data-aos-delay="<?= 300 + $key * 150 ?>"
                        href="<?= $sponsor['link'] ?>" 
                        target="_blank"
                    >
                        <img 
                            src="<?= $sponsor['logo']['url'] ?>" 
                            alt="<?= $sponsor['logo']['alt'] ?>"
                        >
                    </a>
                <?php else : ?>
                    <div 
                        class="sponsor"
                        data-aos="zoom-in"
                        data-aos-delay="<?= 300 + $key * 150 ?>"
                    >
                        <img 
                            src="<?= $sponsor['logo']['url'] ?>" 
                            alt="<?= $sponsor['logo']['alt'] ?>"
                        >
                    </div>
                <?php endif ?>
            <?php endforeach; ?>
        </div>   
    </div>
</section>

<?php get_footer(); ?>