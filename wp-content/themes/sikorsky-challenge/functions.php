<?php
/**
 * sikorsky-challenge functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sikorsky-challenge
 */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'post_id'       => 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Header',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
		'post_id'       => 'header-settings',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
		'post_id'       => 'footer-settings',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> '404 page',
		'menu_title'	=> '404 page',
		'parent_slug'	=> 'theme-general-settings',
		'post_id'       => '404-settings',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Form success message',
		'menu_title'	=> 'Form success message',
		'parent_slug'	=> 'theme-general-settings',
		'post_id'       => 'form-success-message-settings',
		'redirect'		=> false
	));
}

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function sikorsky_challenge_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on sikorsky-challenge, use a find and replace
		* to change 'sikorsky-challenge' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'sikorsky-challenge', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'sikorsky-challenge' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'sikorsky_challenge_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'sikorsky_challenge_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sikorsky_challenge_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sikorsky_challenge_content_width', 640 );
}
add_action( 'after_setup_theme', 'sikorsky_challenge_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sikorsky_challenge_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'sikorsky-challenge' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'sikorsky-challenge' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'sikorsky_challenge_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sikorsky_challenge_scripts() {
	wp_enqueue_style( 'sikorsky-challenge-style', get_stylesheet_uri(), array(), _S_VERSION );
	
	/*css*/
	wp_enqueue_style( 'normalize-css', get_template_directory_uri() . '/assets/css/normalize.css', array("sikorsky-challenge-style"), filemtime(get_stylesheet_directory() . '/assets/css/normalize.css'));
	wp_enqueue_style( 'main-css', get_template_directory_uri() . '/assets/css/main.css', array("sikorsky-challenge-style"), filemtime(get_stylesheet_directory() . '/assets/css/main.css'));

	if (is_front_page()) {
		wp_enqueue_style( 'homepage-css', get_template_directory_uri() . '/assets/css/homepage.css', array("sikorsky-challenge-style"), filemtime(get_stylesheet_directory() . '/assets/css/homepage.css'));
		wp_enqueue_style( 'swiper-css-lib', get_template_directory_uri() . '/assets/libs/swiper/swiper-bundle.min.css', array("sikorsky-challenge-style"), filemtime(get_stylesheet_directory() . '/assets/libs/swiper/swiper-bundle.min.css'));
	}

	if (is_page('blog') || is_category() || is_tag()) {
		wp_enqueue_style( 'blog.css', get_template_directory_uri() . '/assets/css/blog.css', array("sikorsky-challenge-style"), filemtime(get_stylesheet_directory() . '/assets/css/blog.css'));
	}

	if (is_single()) {
		wp_enqueue_style( 'post-css', get_template_directory_uri() . '/assets/css/post.css', array("sikorsky-challenge-style"), filemtime(get_stylesheet_directory() . '/assets/css/post.css'));
	}

	if (is_page_template('templates/template-contact.php')) {
		wp_enqueue_style( 'contact.css', get_template_directory_uri() . '/assets/css/contact.css', array("sikorsky-challenge-style"), filemtime(get_stylesheet_directory() . '/assets/css/contact.css'));
	}

	if (is_404()) {
		wp_enqueue_style( '404-css', get_template_directory_uri() . '/assets/css/404.css', array("sikorsky-challenge-style"), filemtime(get_stylesheet_directory() . '/assets/css/404.css'));
	}

	/*js*/
	wp_enqueue_script('index-js', get_template_directory_uri() . '/js/index-min.js', array('jquery'), '',true, filemtime(get_stylesheet_directory() . '/js/index-min.js'));

	if (is_front_page()) {
		wp_enqueue_script('swiper-js-lib', get_template_directory_uri() . '/assets/libs/swiper/swiper-bundle.min.js', array('jquery'), '',true, filemtime(get_stylesheet_directory() . '/assets/libs/swiper/swiper-bundle.min.js'));
		wp_enqueue_script('homepage-js', get_template_directory_uri() . '/js/homepage-min.js', array('jquery'), '',true, filemtime(get_stylesheet_directory() . '/js/homepage-min.js'));
	}

	if (is_page('blog') || is_category() || is_tag()) {
		wp_enqueue_script('blog-js', get_template_directory_uri() . '/js/blog-min.js', array('jquery'), '',true, filemtime(get_stylesheet_directory() . '/js/blog-min.js'));
	}

	if (is_page_template('templates/template-contact.php')) {
		wp_enqueue_script('form-js', get_template_directory_uri() . '/js/form-min.js', array('jquery'), '',true, filemtime(get_stylesheet_directory() . '/js/form-min.js'));
		wp_enqueue_script('map-js', get_template_directory_uri() . '/js/map-min.js', array('jquery'), '',true, filemtime(get_stylesheet_directory() . '/js/map-min.js'));
	}

	if (is_single()) {
		wp_enqueue_script('post-js', get_template_directory_uri() . '/js/post-min.js', array('jquery'), '',true, filemtime(get_stylesheet_directory() . '/js/post-min.js'));
	}
}
add_action( 'wp_enqueue_scripts', 'sikorsky_challenge_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

