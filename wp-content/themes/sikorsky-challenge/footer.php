<?php
	$footer = get_field('content', 'footer-settings');
?>

	</main>
	
	<footer class="footer">
		<div class="footer-top">
			<div class="wrapper">
				<a class="footer-logo" href="/">
					<img 
						src="<?= $footer['logo']['url'] ?>"
						alt="<?= $footer['logo']['alt'] ?>"
					>
				</a>

				<div class="menus-wrapper">
					<div class="menu contacts">
						<p class="h3-title menu__title">
							<?= $footer['contacts_title'] ?>
						</p>
						<a 
							class="contacts__item"
							target="_blank" 
							rel="nofollow noopener" 
							href=<?= $footer['address']['link'] ?>
						>
							<i>
								<svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M6 0C4.40935 0.00211004 2.88445 0.634929 1.75969 1.75969C0.634929 2.88445 0.00211004 4.40935 0 6C0 10.3075 5.59 15.7025 5.8275 15.93C5.8737 15.9749 5.93558 16 6 16C6.06442 16 6.1263 15.9749 6.1725 15.93C6.41 15.7025 12 10.3075 12 6C11.9979 4.40935 11.3651 2.88445 10.2403 1.75969C9.11555 0.634929 7.59065 0.00211004 6 0V0ZM6 8.75C5.4561 8.75 4.92442 8.58871 4.47218 8.28654C4.01995 7.98437 3.66747 7.55488 3.45933 7.05238C3.25119 6.54988 3.19673 5.99695 3.30284 5.4635C3.40895 4.93005 3.67086 4.44005 4.05546 4.05546C4.44005 3.67086 4.93005 3.40895 5.4635 3.30284C5.99695 3.19673 6.54988 3.25119 7.05238 3.45933C7.55488 3.66747 7.98437 4.01995 8.28654 4.47218C8.58871 4.92442 8.75 5.4561 8.75 6C8.74956 6.72921 8.45969 7.42843 7.94406 7.94406C7.42843 8.45969 6.72921 8.74956 6 8.75Z" fill="white"></path>
								</svg>
							</i>
							<p><?= $footer['address']['text'] ?></p>
						</a>
						<a 
							class="contacts__item"
							href="tel:<?= $footer['phone'] ?>"
						>
							<i>
								<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M13.6965 10.68C13.2595 10.3135 10.6965 8.6905 10.2705 8.765C10.0705 8.8005 9.9175 8.971 9.508 9.4595C9.31862 9.69994 9.11065 9.92514 8.886 10.133C8.47439 10.0336 8.07597 9.88588 7.699 9.693C6.22064 8.97325 5.02632 7.77857 4.307 6.3C4.11412 5.92303 3.96643 5.52461 3.867 5.113C4.07486 4.88835 4.30006 4.68038 4.5405 4.491C5.0285 4.0815 5.1995 3.9295 5.235 3.7285C5.3095 3.3015 3.685 0.7395 3.32 0.3025C3.167 0.1215 3.028 0 2.85 0C2.334 0 0 2.886 0 3.26C0 3.2905 0.05 6.295 3.8445 10.1555C7.705 13.95 10.7095 14 10.74 14C11.114 14 14 11.666 14 11.15C14 10.972 13.8785 10.833 13.6965 10.68Z" fill="white"/>
								</svg>
							</i>
							<p><?= $footer['phone'] ?></p>
						</a>
						<a 
							class="contacts__item"
							href="mailto:<?= $footer['email'] ?>"
						>
							<i>
								<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
									<g clip-path="url(#clip0_1687_1434)">
										<path d="M9.33716 9.80207C8.93909 10.0674 8.47672 10.2077 8 10.2077C7.52331 10.2077 7.06094 10.0674 6.66288 9.80207L0.106531 5.43103C0.0701562 5.40678 0.0347187 5.3815 0 5.3555V12.5179C0 13.3391 0.666406 13.9908 1.47291 13.9908H14.5271C15.3482 13.9908 16 13.3244 16 12.5179V5.35547C15.9652 5.38153 15.9297 5.40688 15.8932 5.43116L9.33716 9.80207Z" fill="white"></path>
										<path d="M0.626562 4.65132L7.18291 9.02238C7.43109 9.18785 7.71553 9.27057 7.99997 9.27057C8.28444 9.27057 8.56891 9.18782 8.81709 9.02238L15.3734 4.65132C15.7658 4.38991 16 3.95241 16 3.48022C16 2.66831 15.3395 2.00781 14.5276 2.00781H1.47241C0.660531 2.00784 0 2.66834 0 3.481C0 3.95241 0.23425 4.38991 0.626562 4.65132Z" fill="white"></path>
									</g>
									<defs>
										<clipPath id="clip0_1687_1434">
											<rect width="16" height="16" fill="white"></rect>
										</clipPath>
									</defs>
								</svg>
							</i>
							<p><?= $footer['email'] ?></p>
						</a>
					</div>
					<nav class="menu">
						<div class="h3-title menu__title">
							<?= $footer['navigation_title'] ?>
						</div>
						<?php
							wp_nav_menu(
								array(
									'menu' => 'menu',
									'container' => null,
								)
							);
						?>
					</nav>
					<div class="menu">
						<div class="h3-title menu__title">
							<?= $footer['follow_us_title'] ?>
						</div>
						<div class="socials">
							<a href="<?= $footer['youtube_link'] ?>" target="_blank" rel="nofollow noopener">
								<i>
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 310 310" style="enable-background: new 0 0 310 310;" xml:space="preserve">
										<g id="XMLID_822_">
											<path id="XMLID_823_" d="M297.917,64.645c-11.19-13.302-31.85-18.728-71.306-18.728H83.386c-40.359,0-61.369,5.776-72.517,19.938   C0,79.663,0,100.008,0,128.166v53.669c0,54.551,12.896,82.248,83.386,82.248h143.226c34.216,0,53.176-4.788,65.442-16.527   C304.633,235.518,310,215.863,310,181.835v-53.669C310,98.471,309.159,78.006,297.917,64.645z M199.021,162.41l-65.038,33.991   c-1.454,0.76-3.044,1.137-4.632,1.137c-1.798,0-3.592-0.484-5.181-1.446c-2.992-1.813-4.819-5.056-4.819-8.554v-67.764   c0-3.492,1.822-6.732,4.808-8.546c2.987-1.814,6.702-1.938,9.801-0.328l65.038,33.772c3.309,1.718,5.387,5.134,5.392,8.861   C204.394,157.263,202.325,160.684,199.021,162.41z" />
										</g>
									</svg>
								</i>
							</a>
							<a href="<?= $footer['facebook_link'] ?>" target="_blank" rel="nofollow noopener">
								<i>
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 23.101 23.101" style="enable-background:new 0 0 23.101 23.101;" xml:space="preserve">
										<g>
											<path d="M8.258,4.458c0-0.144,0.02-0.455,0.06-0.931c0.043-0.477,0.223-0.976,0.546-1.5c0.32-0.522,0.839-0.991,1.561-1.406   C11.144,0.208,12.183,0,13.539,0h3.82v4.163h-2.797c-0.277,0-0.535,0.104-0.768,0.309c-0.231,0.205-0.35,0.4-0.35,0.581v2.59h3.914   c-0.041,0.507-0.086,1-0.138,1.476l-0.155,1.258c-0.062,0.425-0.125,0.819-0.187,1.182h-3.462v11.542H8.258V11.558H5.742V7.643   h2.516V4.458z"/>
										<g>
									</svg>
								</i>
							</a>
							<a href="<?= $footer['tiktok_link'] ?>" target="_blank" rel="nofollow noopener">
								<i>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2859 3333" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd"><path d="M2081 0c55 473 319 755 778 785v532c-266 26-499-61-770-225v995c0 1264-1378 1659-1932 753-356-583-138-1606 1004-1647v561c-87 14-180 36-265 65-254 86-398 247-358 531 77 544 1075 705 992-358V1h551z"></path></svg>
								</i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="wrapper">
				<p class="copyright">
					<?= $footer['copyright'] ?>
				</p>
				<?= $footer['developed_by'] ?>
			</div>
		</div>
		<button class="scroll-up" aria-label="Scroll up">
			<svg viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M4 17.824c-.006.23.08.454.246.644.166.19.404.335.681.414.277.08.58.091.865.032.286-.06.54-.187.728-.364l7.457-6.751 7.454 6.751c.116.122.264.225.433.301.17.076.357.125.552.142.194.017.391.003.579-.041.187-.045.36-.12.509-.218.149-.099.27-.22.354-.357a.884.884 0 0 0 .044-.882 1.123 1.123 0 0 0-.318-.378l-8.526-7.73a1.444 1.444 0 0 0-.486-.286A1.776 1.776 0 0 0 13.976 9c-.206 0-.41.035-.596.101a1.445 1.445 0 0 0-.487.286l-8.534 7.73a.983.983 0 0 0-.359.707Z" fill="#1E284C"></path>
			</svg>
		</button>
	</footer>
</div>

<?php wp_footer(); ?>

<?php if (is_front_page()) : ?>
	<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
	<script>
		AOS.init({
			// once: true,
			duration: 500,
			delay: 300,
		});
	</script>
<?php endif; ?>

</body>
</html>