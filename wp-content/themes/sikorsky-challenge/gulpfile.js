const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const minifyCSS = require('gulp-minify-css');
const minify = require("gulp-babel-minify");
const rename = require("gulp-rename");
const babel = require('gulp-babel');

function swallowError(error) {
    console.log(error.toString())
    this.emit('end')
}

gulp.task('scss', function () {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass())
        .on('error', swallowError)
        .pipe(autoprefixer('last 2 versions'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./assets/css'))
        .pipe(browserSync.reload({ stream: true }));
})

gulp.task('js', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(minify({
            mangle: {
                keepClassName: true
            },
        }))
        .pipe(rename({
            suffix: "-min",
            extname: ".js"
        }))
        .pipe(gulp.dest('./js'))
})

gulp.task('watch', function () {
    browserSync.init({
        proxy: {
            target: 'https://therapieeffekt.lndo.site/',
            ws: false
        }
    });
    gulp.watch(['./src/scss/**/*.scss'], gulp.series('scss'));
    gulp.watch(['./src/js/**/*.js'], gulp.series('js'));
    gulp.watch('./*.html').on('change', browserSync.reload);
});

gulp.task('styles', gulp.series('scss'));
gulp.task('js', gulp.series('js'));
gulp.task('default', gulp.series('watch'));