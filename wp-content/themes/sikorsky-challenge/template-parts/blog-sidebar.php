<?php
	$sidebar = get_field('blog_sidebar', 'theme-general-settings');
?>

<aside class="blog-sidebar">
	<div class="categories">
		<div class="title">
			<i></i>
			<h3><?= $sidebar['categories_title'] ?></h3>
		</div>
		<ul>
			<?php
				wp_nav_menu(
					array(
						'menu' => 'categories',
						'container' => null,
					)
				);
			?>
		</ul>
	</div>
	<div class="tags">
		<div class="title">
			<i></i>
			<h3><?= $sidebar['tags_title'] ?></h3>
		</div>
		<div class="tags__list">
			<?php
				wp_nav_menu(
					array(
						'menu' => 'tags',
						'container' => null,
					)
				);
			?>
		</div>
	</div>
</aside>
