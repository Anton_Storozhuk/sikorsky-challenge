<?php 
    $the_query = new WP_Query( array(
        'posts_per_page' => 3,
        'post__not_in' => array( $post->ID )
    )); 

    $sidebar = get_field('post_sidebar', 'theme-general-settings');
?>

<?php if ( $the_query->have_posts() ) : ?>
    <div class="post-sidebar">
        <h3>
            <?= $sidebar['title'] ?>
        </h3>
        
        <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <article class="article article--min">
                    <a class="article__image" href="<?= the_permalink() ?>">
                        <?= the_post_thumbnail() ?>
                    </a>
                    <div class="article__text-container">
                        <div class="article__categories">
                            <?= the_category(' | ') ?>
                        </div>
                        <h4>
                            <a href="<?= the_permalink() ?>">
                                <?= get_the_title() ?>
                            </a>
                        </h4>
                    </div>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
<?php endif; ?>