jQuery(function ($) {
    // $('#tel').keypress(function(key) {
    //     if(key.charCode < 48 || key.charCode > 57) return false;
    // })

    $('#tel').on('keypress', function (e) {
        let text = String.fromCharCode(e.which)
        text.replace(/\s\s+/g, ' ');
        const pattern = /[0-9()+-\s]/

        return pattern.test(text)
    })

    function showMessage() {
		$('.form-container, .success')
            .addClass('sent')
            .delay(5000)
            .queue(function () {
                $('.form-container, .success').removeClass('sent').dequeue();
            })
		scrollToMessage()
		$('.form-container form').trigger('reset')
	}

	function scrollToMessage() {
		$('html, body').animate({
			scrollTop: $('.contact__content').offset().top
		}, 1000, 'swing');
	}

	document.addEventListener('wpcf7mailsent', function (e) {
		e.preventDefault();
		showMessage();
	}, false);

	document.addEventListener('wpcf7mailfailed', function (e) {
		e.preventDefault();
		$('.mailfailed-error').addClass('show')
	}, false);
});
