jQuery(function ($) {	
    let mainSlider = new Swiper('.main-slider', {
		slidesPerView: 1,
		spaceBetween: 16,
		loop: true,
		speed: 500,
		effect: 'fade',
		fadeEffect: {
			crossFade: true
		},
		autoplay: {
		  delay: 7000,
		  disableOnInteraction: false,
		},
		pagination: {
			el: '.main-slider-pagination',
			type: 'bullets',
            clickable: true,
		},
		keyboard: {
			enabled: true,
		},
		on: {
			init() {
			  this.el.addEventListener('mouseenter', () => {
				this.autoplay.stop();
			  });
		
			  this.el.addEventListener('mouseleave', () => {
				this.autoplay.start();
			  });
			}
		},
	})
});