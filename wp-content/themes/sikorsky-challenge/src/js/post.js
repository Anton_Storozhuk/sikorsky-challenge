jQuery(function ($) {
    $(document).ready(function () {
        $('.share').click(function () {
            window.open($(this).attr('href'), '', 'width=600, height=400');
            return false;
        });

        $('.mobile-social').click(function () {
            if (navigator.share) {
                navigator.share({
                    title: 'WebShare API Demo',
                    url: window.location.href
                }).then(() => {
                    console.log('Thanks for sharing!'); 
                })
                    .catch(console.error);
            } else {
                // fallback
            }
        });

        $('.wp-block-file a:first-of-type()').attr('target','_blank');
    });
});