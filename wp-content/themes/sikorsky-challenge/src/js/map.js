// Initialize and add the map
function google_maps_init() {
    'use strict';
    // The location of Uluru
    var uluru = { lat: 52.045513, lng: 8.509458 };
    // The map, centered at Uluru
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: uluru,
        styles: [
            {
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#f5f5f5',
                    },
                ],
            },
            {
                elementType: 'labels.icon',
                stylers: [
                    {
                        visibility: 'off',
                    },
                ],
            },
            {
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        color: '#616161',
                    },
                ],
            },
            {
                elementType: 'labels.text.stroke',
                stylers: [
                    {
                        color: '#f5f5f5',
                    },
                ],
            },
            {
                featureType: 'administrative.land_parcel',
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        color: '#bdbdbd',
                    },
                ],
            },
            {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#eeeeee',
                    },
                ],
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        color: '#757575',
                    },
                ],
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#e5e5e5',
                    },
                ],
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        color: '#9e9e9e',
                    },
                ],
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#ffffff',
                    },
                ],
            },
            {
                featureType: 'road.arterial',
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        color: '#757575',
                    },
                ],
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#dadada',
                    },
                ],
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        color: '#616161',
                    },
                ],
            },
            {
                featureType: 'road.local',
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        color: '#9e9e9e',
                    },
                ],
            },
            {
                featureType: 'transit.line',
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#e5e5e5',
                    },
                ],
            },
            {
                featureType: 'transit.station',
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#eeeeee',
                    },
                ],
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [
                    {
                        color: '#c9c9c9',
                    },
                ],
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [
                    {
                        color: '#9e9e9e',
                    },
                ],
            },
        ],
    });
    // The marker, positioned at Uluru
    var icon = {
        url: document.getElementById('map').getAttribute('data-marker'),
        scaledSize: new google.maps.Size(26.8, 37),
    };
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: icon,
    });
}

function google_maps_lazyload(api_key) {
    'use strict';

    if (api_key) {
        var options = {
            rootMargin: '100px',
            threshold: 0,
        };

        var map = document.getElementById('map');

        var observer = new IntersectionObserver(function (entries, self) {
            // Intersecting with Edge workaround https://calendar.perfplanet.com/2017/progressive-image-loading-using-intersection-observer-and-sqip/#comment-102838
            var isIntersecting =
                typeof entries[0].isIntersecting === 'boolean'
                    ? entries[0].isIntersecting
                    : entries[0].intersectionRatio > 0;
            if (isIntersecting) {
                var mapsJS = document.createElement('script');
                mapsJS.src =
                    'https://maps.googleapis.com/maps/api/js?callback=google_maps_init&key=' +
                    api_key;
                document.getElementsByTagName('head')[0].appendChild(mapsJS);
                self.unobserve(map);
            }
        }, options);

        observer.observe(map);
    }
}

google_maps_lazyload('AIzaSyDCtGPfIsgyx_owOy9cUcD3xiYGnpLoesI');