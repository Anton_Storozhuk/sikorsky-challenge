jQuery(function ($) {
	/**
	 * menu
	 */
    function showMobileMenu() {
        if($('.header-menu').hasClass('active')) {
            $('body').removeClass('no-scroll');
            $('.header-menu, .burger, .overlay').removeClass('active');
        } else {
            $('body').addClass('no-scroll');
            $('.header-menu, .burger, .overlay').addClass('active');
        }
	}

	$('.burger, .overlay').on('click', function (e) {
		showMobileMenu();
	});

    /**
	 * scroll up
	 */
    $('.scroll-up').on('click', function (e) {
        e.preventDefault()
        $('html, body').animate({ scrollTop: 0 }, 1000)
    })

    $(window).scroll(function () {
        if ($(window).scrollTop() >= $(window).height()) {
            $('.scroll-up').addClass('show')
        } else {
            $('.scroll-up').removeClass('show')
        }
    });
});