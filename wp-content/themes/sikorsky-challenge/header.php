<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @see https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="ie=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta charset="UTF-8" />
	<meta name="format-detection" content="telephone=no" />
	<meta http-equiv="x-rim-auto-match" content="none" />

	<?php if (is_front_page()) : ?>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<?php endif; ?>
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<?php
    $header = get_field('content', 'header-settings');
?>

<div id="page" class="page">
	<header class="header">
		<div class="wrapper">
			<a class="header-logo" href="/">
				<img 
					src="<?= $header['logo']['url'] ?>"
					alt="<?= $header['logo']['alt'] ?>"
				>
			</a>
			<nav class="header-menu">
				<?php
					wp_nav_menu(
						array(
							'menu' => 'menu',
							'container' => null,
						)
					);
				?>
				<a class="btn btn-main header-btn" href="<?= $header['button']['url'] ?>" target="_blank">
					<?= $header['button']['title'] ?>
				</a>
			</nav>
			<div class="burger">
				<div class="burger__item burger__item--1"></div>
				<div class="burger__item burger__item--2"></div>
				<div class="burger__item burger__item--3"></div>
			</div>
			<div class="overlay"></div>
		</div>
	</header>

	<main>
