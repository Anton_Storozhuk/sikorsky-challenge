<?php
	get_header(); 

    $tag = get_queried_object();
    $slug = $tag->slug;
    $name = $tag->name;
?>

<section class="blog">
    <div class="wrapper">
        <div class="blog--left">
            <div class="breadcrumbs tag-breadcrumbs"> 
                <a class="home" href="<?= home_url() ?>">
                    <i>
                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.6765 5.21942C11.6762 5.21915 11.6759 5.21887 11.6756 5.2186L6.78059 0.32373C6.57194 0.11499 6.29453 0 5.99946 0C5.70439 0 5.42698 0.114899 5.21824 0.323639L0.325757 5.21603C0.324109 5.21768 0.322461 5.21942 0.320813 5.22107C-0.107654 5.65201 -0.106921 6.3512 0.322919 6.78104C0.519299 6.97751 0.778668 7.09131 1.05598 7.10321C1.06724 7.10431 1.07859 7.10486 1.09004 7.10486H1.28514V10.7072C1.28514 11.42 1.86512 12 2.57814 12H4.49324C4.68733 12 4.8448 11.8426 4.8448 11.6484V8.82422C4.8448 8.49893 5.10939 8.23434 5.43467 8.23434H6.56425C6.88954 8.23434 7.15412 8.49893 7.15412 8.82422V11.6484C7.15412 11.8426 7.3115 12 7.50569 12H9.42078C10.1338 12 10.7138 11.42 10.7138 10.7072V7.10486H10.8947C11.1897 7.10486 11.4671 6.98996 11.6759 6.78122C12.1062 6.35065 12.1064 5.65027 11.6765 5.21942Z" fill="black"/>
                        </svg>
                    </i>
                </a>
                <span>
                        <a href="/blog">Блог</a>
                    </span>
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
            </div>
            <h1>
                <span>
                    #<?= $name ?>
                </span>
            </h1>
            <div class="articles-list__wrapper articles-list__wrapper--three-rows">
                <?php 
                    $args = array(
                        'posts_per_page' => 4,
                        'orderby' => 'date',
                        'tag' => $slug
                    );

                    $query = new WP_Query( $args );

                    if ( $query->have_posts() ) : 
                        while ( $query->have_posts() ) : 
                            $query->the_post(); 
                            get_template_part( 'template-parts/article', 'article' ); 
                        endwhile; 
                            wp_reset_postdata(); 
                    endif;
                ?>
                        
                <?= do_shortcode ('[ajax_load_more id="1528121329" offset="4" pause="true" tag="'. $slug .'" button_label="Показати більше" container_type="div" post_type="post" posts_per_page="4" scroll="false"]') ?>
            </div>
        </div>
        <div class="blog--right">
            <?php get_template_part( 'template-parts/blog-sidebar', 'blog-sidebar' ); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>